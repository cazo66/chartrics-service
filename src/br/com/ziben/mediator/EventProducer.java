package br.com.ziben.mediator;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.enterprise.event.Event;
import javax.inject.Inject;

/**
 * Classe scheduler que produz um evento a cada X minuto
 * Contrib: ccardozo
 */
@Stateless
public class EventProducer  {
@Inject @WBTimeEvent Event<TimeEvent> timeEvent;

	// a cada 3s, todo minuto, toda hora
    @Schedule(second="*/3", minute="*", hour="*", info="Chartrics Time Event publisher")
    public void produceRegularEvent(Timer t) {
        System.out.println("Call # " );
        TimeEvent event = new TimeEvent();
        timeEvent.fire(event);        
    }
        
}
