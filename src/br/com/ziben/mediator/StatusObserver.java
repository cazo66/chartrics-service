package br.com.ziben.mediator;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author ccardozo
 */
@Singleton
@Startup
@ServerEndpoint("/status")
public class StatusObserver {
    
//     public void onTimeEvent(@Observes @WBTimeEvent TimeEvent msg) {
//         System.out.println("TimeEventObserver:TimeEVent!");
//     }

     private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

     @OnMessage
     public String onMessage(String message, Session session) {
    	 System.out.println(message);
    	 
    	 for (Session peer : peers) {
            if (!peer.equals(session)) {
                try {
                    peer.getBasicRemote().sendText(message);
                } catch (IOException ex) {
                    Logger.getLogger(TimeEventObserver.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return message;
    }

    @OnOpen
    public void onOpen(Session peer) {
        peers.add(peer);
    }
     
    @OnClose
    public void onClose(Session peer) {
        peers.remove(peer);
    }

}
