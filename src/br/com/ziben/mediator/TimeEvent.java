package br.com.ziben.mediator;

import java.util.Date;

/**
 * classe que retorna um timestamp
 * @author ccardozo
 *
 */
public class TimeEvent {
    
    private Date timestamp;

    public TimeEvent() {
        timestamp = new Date();
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }
    
    
    
}
