package br.com.ziben.mediator;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;


@Singleton
@ServerEndpoint("/mediator1")
public class SocketMediator {

//    public void onTimeEvent(@Observes @WBTimeEvent TimeEvent event) {
//        System.out.println("Time Event observed " + event.getTimestamp());
//
//        for (Session peer : peers) {
//            try {
//                peer.getBasicRemote().sendText("Time event: " + event.getTimestamp());
//            } catch (IOException ex) {
//                Logger.getLogger(SocketMediator.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }

    private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

    @OnMessage
    public String onMessage(String message, Session session) {
        System.out.println("mensagem recebida " + message);
        for (Session peer : peers) {
            if (!peer.equals(session)) {
                try {
                    peer.getBasicRemote().sendText(message + " - retweet");
                } catch (IOException ex) {
                    Logger.getLogger(SocketMediator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return "mensagem recebida e processada: "+message;
    }

    @OnOpen
    public void onOpen(Session peer) {
    	System.out.println("onOpen(Session peer...)");
        peers.add(peer);
    }


    
    @OnClose
    public void onClose(Session peer) {
    	System.out.println("onClose(Session peer)...");
       peers.remove(peer);
    }
}

