package br.com.ziben.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

import br.com.ziben.interceptors.SplitBasicAuth;
import br.com.ziben.results.ErrorsResult;
import br.com.ziben.results.SuccessResult;

/**
 * Classe para manipula�ao das coisas de login
 * @author ccardozo
 *
 */
@Path("/login")
public class HandleLoginRest {

	private Logger log = Logger.getLogger("HandleLoginRest");

	@GET
	@GZIP
	// @PermitAll
	@Path("/perform")
	@Produces("application/json")
	public Response perform(@HeaderParam("authorization") String auth) {

		log.debug(">>HandleLoginRest:perform()");

		ResponseBuilder rb = null;
		// simular json de retorno
		Map<String, String> jsonRet = new HashMap<String, String>();
		jsonRet.put("userName", "admin");
		jsonRet.put("firstName", "Administrador");
		jsonRet.put("lastName", "Administrador do Site");
		jsonRet.put("userEmail", "cazo66@gmail.com");
		jsonRet.put("defaultUserFlag", ""); // se tiver vazio, eh adm

		try {
			Map<String, String> userPass = SplitBasicAuth.splitAuth(auth);
			String usuario = userPass.get("name");
			System.out.println("Usuario: " + userPass.get("name"));
			System.out.println("Senha: " + userPass.get("password"));

			// condicional para testar o retorno com erro
			if (usuario.equals("admin")) {
				// retorna 200, req ok
				rb = Response.ok(
						SuccessResult.success("Login enviado HandleLoginRest:perform com sucesso!", (Object) jsonRet),
						MediaType.APPLICATION_JSON);
			} else {
				// retorna 401 Not Authorized
				rb = Response.status(401).entity(ErrorsResult.errors("O login ou senha informados s�o inv�lidos!"));
			}
		} catch (IOException e1) {
			log.error("<<HandleLoginRest:perform erro de IOException", e1);
			// 500 Internal Error
			rb = Response.status(500)
				.entity(ErrorsResult.errors("Erro de Exception durante login", (String) e1.getStackTrace().toString()));
		} catch (Exception e) {
			log.error("<<HandleLoginRest:perform erro de Exception", e);
			rb = Response.status(500)
					.entity(ErrorsResult.errors("Erro de Exception durante login", (String) e.getStackTrace().toString()));
		} finally {
			log.debug("<<HandleLoginRest:perform()");
		}
		return rb.build();
	}
}