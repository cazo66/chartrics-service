package br.com.ziben.rest;

import javax.annotation.security.PermitAll;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

import br.com.ziben.results.ErrorsResult;
import br.com.ziben.results.SuccessResult;

@Path("/password")
public class HandlePasswordRest {

	private static Logger log = Logger.getLogger(HandlePasswordRest.class);

	@PUT
	@GZIP
	@PermitAll
	@Path("/change/{userName}")
	@Produces("application/json")
	public Response perform(@PathParam("userName") String userName, String password) {
		log.debug(">>HandlePasswordRest:change()");

		ResponseBuilder rb = null;

		try {
			System.out.println(userName);
			System.out.println(password);

			rb = Response.ok(SuccessResult.success("Senha alterada com sucesso!"), MediaType.APPLICATION_JSON);
		} catch (Exception e) {
			log.error("<<HandlePasswordRest:change erro de Exception", e);
			rb = Response.status(500).entity(
					ErrorsResult.errors("Erro de Exception durante altera��o de senha", (String) e.getMessage()));
		} finally {
			log.debug("<<HandlePasswordRest:change()");
		}
		return rb.build();
	}
}