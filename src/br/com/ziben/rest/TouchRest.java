package br.com.ziben.rest;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

import br.com.ziben.results.SuccessResult;

/**
 * Classe dummy / exemplo / template para acessos de curiosos; ao final deve ser removida
 * @author ccardozo
 *
 */
@Path("/touch")
public class TouchRest {

	private Logger log = Logger.getLogger("TouchRest");

	@POST
	@GZIP
	@PermitAll
	@Path("/say")
	@Produces("application/json")
	public Response say(String json) {

		log.debug(">>TouchRest>say");

		ResponseBuilder rb = null;
		try {
/*			
			ObjectMapper mapper = new ObjectMapper();
			PolicyEntry politica = mapper.readValue(json, PolicyEntry.class);

			System.out.println("nome politica: -->> " + politica.getCod_politica());

			// Vari�veis Internas
			List<InVariableEntry> variaveisInternas = politica.getVariaveis_entrada();

			for (InVariableEntry variavelInterna : variaveisInternas) {
				System.out.println("Vari�vel Interna --> " + variavelInterna.getCod_variavel());
			}

			// Vari�veisTrabalho
			List<WorkVariableEntry> variaveisTrabalho = politica.getVariaveis_trabalho();

			for (WorkVariableEntry variavelTrabalho : variaveisTrabalho) {
				System.out.println("Vari�vel Trabalho --> " + variavelTrabalho.getCod_variavel());
			}

			// Grid
			GridPolitica gridPolitica = politica.getDes_grid();
			System.out.println("Grid da pol�tica -->> " + gridPolitica.getGrid_politica());

			// A��es
			List<ActionEntry> acoes = politica.getAcoes();

			for (ActionEntry acao : acoes) {
				System.out.println("A��es --> " + acao.getCod_acao());
			}

			// Acessos
			List<AccessEntry> acessos = politica.getAcessos();

			for (AccessEntry acesso : acessos) {
				System.out.println("Acesso -->> " + acesso.getCod_acesso());
				List<LayoutAccessEntry> layoutEntradaAcesso = acesso.getLayout_entrada();
				for (LayoutAccessEntry variavelEntradaAcesso : layoutEntradaAcesso) {
					System.out.println("Variavel Entrada Acesso --> " + variavelEntradaAcesso.getNom_campo());
				}

				List<LayoutAccessEntry> layoutSaidaAcesso = acesso.getLayout_saida();
				for (LayoutAccessEntry variavelSaidaAcesso : layoutSaidaAcesso) {
					System.out.println("Variavel Saida Acesso--> " + variavelSaidaAcesso.getNom_campo());
				}
			}

			// Layout Pol�tica
			PolicyLayoutEntry layoutPolitica = politica.getLayout_politica();
			System.out.println("Layout Pol�tica -->> " + layoutPolitica.getCod_layout());

			List<LayoutPolicyEntry> layoutEntradaPolitica = layoutPolitica.getLayout_entrada();
			for (LayoutPolicyEntry variavelEntrada : layoutEntradaPolitica) {
				System.out.println("Variavel Entrada Politica -->> " + variavelEntrada.getNom_campo());
			}

			List<LayoutPolicyEntry> layoutSaidaPolitica = layoutPolitica.getLayout_saida();
			for (LayoutPolicyEntry variavelSaidaPolitica : layoutSaidaPolitica) {
				System.out.println("Variavel Saida Politica --> " + variavelSaidaPolitica.getNom_campo());
			}

		} catch (UnrecognizedPropertyException e) {
			System.out.println("Erro de tag no JSON: " + e.getMessage());
			// TODO Auto-generated catch block
*/			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			rb = Response.ok(SuccessResult.success("Texto enviado pra Rest com sucesso!"), MediaType.APPLICATION_JSON);
		} catch (Exception e) {
			log.error("<<TouchRest<say deu pau geral! Olhem o estrago abaixo!", e);
			rb = Response.status(500).entity("Um erro inesperado aconteceu, sinto muito!");
		} finally {
			log.debug("<<TouchRest<say");
		}
		return rb.build();
	}
}